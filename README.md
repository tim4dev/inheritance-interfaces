# Inheritance and visibility of interfaces

Run unit test ```UnitTest.kt#inheritanceTest()```

The task is to hide ```Library1``` interface1 behind the facade of ```Library2```

https://stackoverflow.com/q/58318259/345810

https://docs.gradle.org/current/userguide/java_library_plugin.html
