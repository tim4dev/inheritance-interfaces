package soft.example.myapplication

import soft.example.library2.Interface2

class KlassApp constructor(
    private val klass: Interface2
) {
    fun klassAppFun() {
        klass.interface1fun1()
    }
}