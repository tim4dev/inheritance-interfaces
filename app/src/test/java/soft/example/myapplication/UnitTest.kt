package soft.example.myapplication

import org.junit.Test
import soft.example.library2.Klass2

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class UnitTest {

    @Test
    fun inheritanceTest() {
        val klass1 = Klass2()
        val klassApp = KlassApp(klass1)
        klassApp.klassAppFun()
    }
}
